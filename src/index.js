import { drawTaskList } from './todo';
import { drawHeader } from './header';

const root = document.getElementById('root');

root.appendChild(drawHeader());
root.appendChild(drawTaskList());

const ourDate = new Date();
const days = getDays(ourDate);
root.appendChild(drawDays(days));