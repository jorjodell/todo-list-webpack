export function getDays(date) {
  // Временная дата для получения последнего числа
  const tempDate = new Date(
    date.getFullYear(), // год
    date.getMonth() + 1, // следующий месяц
    0 // последнее число текущего месяца
    );
    
    // Получаем последнее число
    const daysCount = tempDate.getDate();
    
    // Меняем числоа на первое
    tempDate.setDate(1);
    
    // Получаем первый день недели
    const firstDay = tempDate.getDay();
    // Меняем на наш формат дней недели
    const ourFirstDay = firstDay === 0 ? 6 : firstDay - 1;
    const days = [];
    
    // Добавляем пустые значения до начала календаря
    for(let i = 0; i < ourFirstDay; i++) {
      days.push("");
    }
    
    // Добавляем сами числа
    for(let i = 1; i <= daysCount; i++) {
      days.push(i);
    }

    return days;
}

export function drawDays(days) {
  const daysNode = document.createElement('div');
  daysNode.classList.add('days');

  for(let i = 0; i <= days.length; i++) {
    const day = document.createElement('button');
    day.classList.add('day');
    day.textContent = days[i];
    daysNode.appendChild(day);
  }
  return daysNode;
}

