export function drawToggle() {
  const toggle = document.createElement('div');
  toggle.classList.add('toggle');
  const toggleBtn = document.createElement('button');
  toggleBtn.classList.add('toggle__btn');
  toggleBtn.textContent = 'Monthly';

  const toggleBtn2 = document.createElement('button');
  toggleBtn2.classList.add('toggle__btn');
  toggleBtn2.textContent = 'Daily';

  toggleBtn.addEventListener('click', function() {
    toggle.classList.remove('toggle--daily')
  })
  toggleBtn2.addEventListener('click', function() {
    toggle.classList.add('toggle--daily')
  })
  
  const toggleBg = document.createElement('div');
  toggleBg.classList.add('toggle__bg');

  toggle.appendChild(toggleBtn);
  toggle.appendChild(toggleBtn2);
  toggle.appendChild(toggleBg);

  return toggle;
}

export function drawHeader() {
  const header = document.createElement('header');
  header.classList.add('header');
  const logo = document.createElement('img');
  logo.src = './images/logo.svg';
  logo.classList.add('logo');
  header.appendChild(logo);
  header.appendChild(drawToggle());
  return header;
}

