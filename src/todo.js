
export function drawTask(title, date, status = false) {
  const task = document.createElement('article');
  task.classList.add('task');

  const taskTitle = document.createElement('h1');
  taskTitle.classList.add('task__title');
  taskTitle.textContent = title;

  const taskDate = document.createElement('div');
  taskDate.classList.add('task__date');
  taskDate.textContent = date;

  const taskStatus = document.createElement('div');
  taskStatus.classList.add('task__status');
  taskStatus.textContent = status ? 'Выполнено' : 'Не выполнено';

  task.appendChild(taskStatus);
  task.appendChild(taskTitle);
  task.appendChild(taskDate);

  return task;
}

export const taskList = [
  {title: 'Купить молоко', status: false, date: '25/01/2020'},
  {title: 'Прочитать книгу', status: true, date: '26/01/2020'},
  {title: 'Заправить кровать', status: false, date: '24/01/2020'},
  {title: 'Заправить кровать', status: false, date: '24/01/2020'},
  {title: 'Заправить кровать', status: false, date: '24/01/2020'},
  {title: 'Заправить кровать', status: false, date: '24/01/2020'},
]

export function drawTaskList() {
  const taskListNode = document.createElement('div');
  taskListNode.classList.add('task-list');
  taskList.forEach(function(task) {
    taskListNode.appendChild(
      drawTask(task.title, task.date, task.status)
    );
  })
  return taskListNode;
}
